describe('getAllLocations', function(){
    beforeEach(module('MyApp'));
    
describe('data', function() {
    
    var data;
    beforeEach(inject(function($getAllLocations) {
        data = $getAllLocations('data', {});
        
    }));
             
    it('should display list of rooms', function() {
        expect(data(Starbucks)).toBe('Starbucks'); 
    });

});

});
