// Home controller and injectors
/*global MyApp*/
MyApp.controller('thetaCtrl', ['$scope', '$location', '$window', 'LoginService', "$http", "myConfig", "$stateParams", function ($scope, $location, $window, $LoginService, $http, myConfig, $stateParams) {

    // Declaration
	$scope.authorized = $LoginService.verify();
	$scope.error = '';
	
	$scope.getAllLocations = function() {
        $http({
            url: myConfig.baseURL + "/api/getAllLocations", 
            method: "get",
         }).success(function(data){
            $scope.AllLocations = data.data;
            $scope.error = '';
        }).error(function(err){
            $scope.error = "Error: " + err.message
            $scope.AllLocations = '';
        });
    };
      
    	$scope.getAllSkills = function() {
        $http({
            url: myConfig.baseURL + "/api/getAllSkills", 
            method: "get",
         }).success(function(data){
            $scope.AllSkills = data.data;
            $scope.error = '';
        }).error(function(err){
            $scope.error = "Error: " + err.message
            $scope.AllSkills = '';
        });
    };
    
    
    	$scope.getPersonEmail = function(email) {
        $http({
            url: myConfig.baseURL + "/api/lookUpPerson/" + email, 
            method: "get",
         }).success(function(data){
            $scope.PersonEmail = data.data;
        });
        
    };
    
    	$scope.getPersonBySkill = function(skill) {
        $http({
            url: myConfig.baseURL + "/api/getPersonBySkill/" + skill,
            method: "get",
         }).success(function(data){
            $scope.PersonBySkill = data.data;
        });
    };

        $scope.getAvailabilityByPerson = function(email) {
        $http({
            url: myConfig.baseURL + "/api/getAvailabilityByPerson/" + email, 
            method: "get",
         }).success(function(data){
            $scope.AvailabilityByPerson = data.data;
        });
    };

        $scope.getPersonalAppointments = function(email) {
        $http({
            url: myConfig.baseURL + "/api/getPersonalAppointments/" + email, 
            method: "get",
            headers: {
                'token': $window.sessionStorage.token
            }
         }).success(function(data){
            $scope.PersonalAppointments = data.data;
            $scope.error = '';
        }).error(function(err){
            $scope.error = "Error: " + err.message
            $scope.PersonalAppointments = '';
        });
    };
}]);