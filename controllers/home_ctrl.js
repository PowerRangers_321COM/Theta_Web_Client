// Home controller and injectors
/*global MyApp */
MyApp.controller('homeCtrl', ['$scope', '$location', '$window', 'LoginService', "$http", "myConfig", function ($scope, $location, $window, $LoginService, $http, myConfig) {

    // Declaration
	$scope.authorized = $LoginService.verify();

}]);