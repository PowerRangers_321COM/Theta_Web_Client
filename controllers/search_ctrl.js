// Home controller and injectors
/*global MyApp*/
MyApp.controller('searchCtrl', ['$scope', '$location', '$window', 'LoginService', "$http", "myConfig", "$stateParams", function ($scope, $location, $window, $LoginService, $http, myConfig, $stateParams) {

    // Declaration
	$scope.authorized = $LoginService.verify();
	

$scope.getPersonEmail = function(email) {
        $http({
            url: myConfig.baseURL + "/api/lookUpPerson/" + email, 
            method: "get",
         }).success(function(data){
            $scope.PersonEmail = data.data;
        });
        
    };
    
    	$scope.getPersonBySkill = function(skill) {
        $http({
            url: myConfig.baseURL + "/api/getPersonBySkill/" + skill,
            method: "get",
         }).success(function(data){
            $scope.PersonBySkill = data.data;
        });
    };

        $scope.getAvailabilityByPerson = function(email) {
        $http({
            url: myConfig.baseURL + "/api/getAvailabilityByPerson/" + email, 
            method: "get",
         }).success(function(data){
            $scope.AvailabilityByPerson = data.data;
        });
    };


        $scope.getPersonalappointments = function(email) {
        $http({
            url: myConfig.baseURL + "/api/getPersonalappointments/" + email, 
            method: "get",
         }).success(function(data){
            $scope.Personalappointments = data.data;
            $scope.error = '';
        }).error(function(err){
            $scope.error = "Error: " + err.message
            $scope.PersonalAppointments = '';
        });
    };
    
}]);
