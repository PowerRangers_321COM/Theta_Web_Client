
describe("Unit: Testing Controller", function() {
  var service, httpBackend, controller, scope, service, $window;
  describe("Home Ctrl:", function() {

    beforeEach(module('ngRoute'));
    beforeEach(module('ui.router'));
    beforeEach(module('ngSanitize'));
    beforeEach(module('UserService'));
    beforeEach(module('myApp'));

    beforeEach(inject(function($rootScope, $injector, $httpBackend, $templateCache) {
      scope = $rootScope.$new();
      var _ctrl = $injector.get('$controller');
      service = $injector.get('LoginService');
      httpBackend = $httpBackend;

      $templateCache.put('templates/home.html', 'templates/home.html');
      spyOn(service, 'verify').and.returnValue(true);
      controller = _ctrl('homeCtrl', {'$scope' : scope ,'LoginService' : service});
    }));

    
    afterEach(function() {
      httpBackend.verifyNoOutstandingExpectation();
      httpBackend.verifyNoOutstandingRequest();
    });
  })
});