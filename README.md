PowerRangers Project
Overview
We have developed a website for student help or support. Our website is running on http://powerrangers-mthembow.c9users.io/Theta_Web_Client/index.html
Our user stories are found on https://trello.com/b/dycWKxP9/321com
Our gitlab repository is https://gitlab.com/PowerRangers_321COM/Theta_Web_Client.git

Our group has collaborated with Cortex and gets data from http://cortexapi.ddns.net:8080/api

Group Members               Role
Ousman                      Leader/ Idea/ Encourager
Awat                        Compromiser
Wendy                       Recorder / Summariser Clarifify
Constantinos                Summariser / Idea
Theodore                    Evaluator / Summariser / Encourager


The project is developed to help students with their studies. The project has been developed in AngularJs, CSS and HTML. 
The website allows the user, who is a student to do the following:

•	The student will log onto the website by registering their credentials
•	The student will search and view for availability of lecturers as well as sessions. 
    This gives the student an idea of who is available and the support that is available at that time.
•	The student books times for assistance with a lecturer. A notification of confirmation of booking is sent to the student 
    and that time and slot is made unavailable to other users
•	The student is able to cancel a booking and a confirmation email is sent to the student. Cancellation renders the slot / session
    available for others to book it.
•	Changes to the booking will only be facilitated depending on the availability of the lecturer and if the times to change to are free, 
    otherwise the change will amount to a cancellation.
•	The student is able to view the bookings for a particular lecturer.

We have successfully gone through four sprints.
1. As a student can l view the availability of a lecturer so that l can book for a sesseion
2. As a student can l search for a lecturer with a particular skills for availability
3. As a lecturer can l register and add my skill to be available to student
4. As a student can l register new user account and be able to log in and out
 

A student is able to see the availability of support that suites their time. The user can see the room and lecturer allocation.

The project shows the locations. User is able to make searches on the following features:
1. Able to sarch by lecturer email address
2. Able to search by lecturer skills and availabity.


