// Angular app declaration, injecting modules and services
/*global angular */
var MyApp = angular.module("MyApp", ['ui.router', 'ngRoute', 'ngSanitize', 'UserService']);
MyApp.constant('myConfig',
	{
	
		baseURL: 'http://cortexapi.ddns.net:8080'

	});

// Token interceptor
MyApp.config(function ($httpProvider) {
    $httpProvider.interceptors.push('TokenInterceptor');
});

// Routes declaration
MyApp.config(function($stateProvider, $urlRouterProvider){

	$stateProvider
	
		.state('home',{
			url:"/",
			views: {
			  '@': {            
				templateUrl: "templates/home.html",
				controller: 'homeCtrl',
				access: { requiredLogin: false }
			  }
			}
		})
		.state('login',{
			url:"/login",
			views: {
			  '@': {            
				templateUrl: "templates/users/login.html",
				controller: 'userCtrl',
				access: { requiredLogin: false }
			  }
			}
		})
		.state('logout',{
			url:"/logout",
			views: {
			  '@': {
			  	templateUrl: "templates/users/logout.html",
				controller: 'userCtrl',
				access: { requiredLogin: true }
			  }
			}
		})
		.state('register',{
			url:"/register",
			views: {
			  '@': {            
				templateUrl: "templates/users/register.html",
				controller: 'userCtrl',
				access: { requiredLogin: false }
			  }
			}
		})
		
		.state('search',{
			url:"/search",
			views: {
			  '@': {            
				templateUrl: "templates/users/search.html",
				controller: 'searchCtrl',
				access: { requiredLogin: false }
			  }
			}
		})
		.state('theta',{
			url:"/theta",
			views: {
			  '@': {            
				templateUrl: "templates/theta.html",
				controller: 'thetaCtrl',
				access: { requiredLogin: false }
			  }
			}
		})

	$urlRouterProvider.otherwise('/');
	
		
})

MyApp.run(function($rootScope, $state) {
$rootScope.$on("$stateChangeStart", 
	function(event, toState, toParams, fromState, fromParams) {
       $state.callbackState = fromState.name       
   });

});
