// Creating service module
/*global angular */
var UserService = angular.module('UserService', [])
// Login services
UserService.factory('LoginService', ["$http", "$window", "myConfig", function($http, $window, $myConfig) {
    return {
        logIn: function(username, password) {
            return $http.post($myConfig.baseURL + '/login', { 'username' : username, 'password' : password}).success(function(data){
                });
        },
 
        register: function(username, password, passwordConfirmation) {
            return $http.post($myConfig.baseURL + '/register', {username: username, password: password, passwordConfirmation: passwordConfirmation }).success(function(data){
                });
        },
        verify: function() {
            if ($window.sessionStorage.token) {
                return true
            }else{
                return false
            }
        }
    }
}]);

// Authentication services
UserService.factory('AuthenticationService', function() {
    var auth = {
        isAuthenticated: false,
        isAdmin: false
    }

    return auth;
});

// Token interceptor services
UserService.factory('TokenInterceptor', ["$q", "$window", "$location", "AuthenticationService", function ($q, $window, $location, $AuthenticationService) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.sessionStorage.token) {
                config.useXDomain = true;
            }
            return config;
        },

        requestError: function(rejection) {
            return $q.reject(rejection);
        },

        response: function (response) {
            if (response != null && response.status == 200 && $window.sessionStorage.token && !$AuthenticationService.isAuthenticated) {
                $AuthenticationService.isAuthenticated = true;
            }
            return response || $q.when(response);
        },

        responseError: function(rejection) {
            if (rejection != null && rejection.status === 401 && ($window.sessionStorage.token || $AuthenticationService.isAuthenticated)) {
                delete $window.sessionStorage.token;
                $AuthenticationService.isAuthenticated = false;
                $location.path("/login");
            }

            return $q.reject(rejection);
        }
    };
}]);